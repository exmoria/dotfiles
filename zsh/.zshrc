# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# ================================ General ================================== #

# Should be called before compinit.
zmodload zsh/complist 

autoload -U compinit promptinit \
    && compinit \
    && promptinit
 
# With hidden files.
_comp_options+=(globdots)

# By default, Ctrl+d will not close your shell if the command line is filled.
# This fixes it. 
exit_zsh() { exit }
zle -N exit_zsh
bindkey '^D' exit_zsh

# History in cache directory.
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

# ================================ Options ================================== #

setopt AUTO_CD                # Automatically cd into typed directory.
setopt AUTO_PUSHD             # Push the current directory visited on the stack.
setopt PUSHD_IGNORE_DUPS      # Do not store duplicates in the stack.
setopt PUSHD_SILENT           # Do not print the directory stack after pushd or popd.
setopt NO_CASE_GLOB           # Make globbing case insensitive.
setopt HIST_EXPIRE_DUPS_FIRST # Expire duplicates first.
setopt HIST_IGNORE_DUPS       # Do not store duplications.
setopt HIST_REDUCE_BLANKS     # Remove blank lines from history.


# ============================== Completions ================================ #

# Define completers.
zstyle ':completion:*' completer _extensions _complete _approximate

# Auto/tab complete.
zstyle ':completion:*' menu select

zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}!- %d (errors: %e) -!%f'
zstyle ':completion:*:*:*:*:descriptions' format '%F{blue}-- %D %d --%f'
zstyle ':completion:*:*:*:*:messages' format ' %F{purple} -- %d --%f'
zstyle ':completion:*:*:*:*:warnings' format ' %F{red}-- no matches found --%f'

# Parameter completion for the dotnet CLI.
_dotnet_zsh_complete() {
  local completions=("$(dotnet complete "$words")")
  reply=( "${(ps:\n:)completions}" )
}

compctl -K _dotnet_zsh_complete dotnet

# =============================== Keybinds ================================== #
### Ref: https://wiki.archlinux.org/title/Zsh?useskinversion=1#Key_bindings 

typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete

if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

# ================================== Misc =================================== #

scaffold_identity() {
  dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design && \
  dotnet add package Microsoft.EntityFrameworkCore.Design && \
  dotnet add package Microsoft.AspNetCore.Identity.EntityFrameworkCore && \
  dotnet add package Microsoft.AspNetCore.Identity.UI && \
  dotnet add package Microsoft.EntityFrameworkCore.SqlServer && \
  dotnet add package Microsoft.EntityFrameworkCore.Tools
}

# ================================ Prompt =================================== #

local P10K="/usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme"
[[ -f $P10K ]] && source $P10K

# ============================ Autosuggestions ============================== #

local FILE="/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
[[ -f $FILE ]] && source $FILE

# ================================ Aliases ================================== #

# make
alias m="make"

# .zshrc
alias zrc='code ~/.config/zsh/.zshrc'

# chezmoi
alias cad='chezmoi add'
alias cap='chezmoi apply'
alias cdiff='chezmoi diff'

# pacman
alias paci='sudo pacman -S'
alias pacu='sudo pacman -Syu'
alias pacr='sudo pacman -Rs'
alias pacs='sudo pacman -Ss'

# yay
alias yu='yay -Syu --combinedupgrade'
alias ys='yay -Ss'
alias yi='yay -S'
alias yr='yay -Rs'

# kubectl
alias k='kubectl'
alias kg='kubectl get'
alias kdel="kubectl delete $1 $2"

# maven
alias mvn='mvnd'

# git
alias gc='git clone'

# docker
alias di="docker images"
alias dia="docker images -a"
alias dirm="docker image remove"
alias dps="docker ps -a" 
alias lsvol="docker volume ls"
alias rmvol='docker volume rm "$(docker volume ls -q)"'
alias dprune="docker system prune -f"
alias dex="docker ps -f 'status=exited'"
dstop() { docker stop $(docker ps -a -q); } # Stop all containers.
drall() { docker rm $(docker ps -a -q); } # Remove all containers.

# youtubedl
alias ydlF='youtube-dl -F'
alias ydla='youtube-dl -x --audio-quality 0'

# Misc
alias mkdir='mkdir -pv'
alias l='ls -ghAFG --color=auto --group-directories-first'
alias ls='ls -ghAFG --color=auto --group-directories-first'
alias grep='grep -iF'
alias dirty='watch grep -e Dirty: -e Writeback: /proc/meminfo'
alias gin='npx gitignosource /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme'

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/exmoria/.sdkman"
[[ -s "/home/exmoria/.sdkman/bin/sdkman-init.sh" ]] && source "/home/exmoria/.sdkman/bin/sdkman-init.sh"
